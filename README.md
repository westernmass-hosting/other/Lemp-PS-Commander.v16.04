# LEMP-PS-Commander
I needed a quick way to manage a LEMP server, so here we are...

This will install or update and configure the following:  nGinx, Pagespeed, PHP 7 (with modules), MySQL, NTP, phpMyAdmin, PostFix, Pure-FTP, Maldet, ClamAV, Fail2Ban

I recommend performing the following before running our installer to ensure a smooth install:
- Run: `dpkg-reconfigure dash` and select NO to force `bash`
- Add a Swapfile by running:
 
```
fallocate -l 8G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon -s
```
- - Once this is done, add: `/swapfile   none    swap    sw    0   0` to /etc/fstab
- Configure UFW

```
ufw allow http
ufw allow https
ufw allow ftp
ufw allow 30000:50000/tcp
ufw allow 30000:50000/udp
ufw allow ssh
ufw enable
```
- If your server has enough RAM, you may also want to consider configuring your caching directories for your sites to reside in a RAMDisk
- - See here: https://www.scalescale.com/tips/nginx/mount-directory-into-ram-memory-better-performance/
- You should probably change the hostname if you have not already set it
- - `hostnamectl set-hostname THENEWHOSTNAME`

## Requirements
- Ubuntu Server 16.04
- - It should work on other `Debian` derivatives, however, I will not support anything but Ubuntu Server 16.04 and up
- Shell access to this server
- Some Coffee, and some patience

## Install:
- Clone this repo: `git clone https://github.com/kpirnie/Lemp-PS-Commander.git && cd LEMP-Command && ./cp`
- - Pay attention, there may be some items that may need your input

## Update:
- Clone this repo: `git clone https://github.com/kpirnie/Lemp-PS-Commander.git && cd LEMP-Command && ./cp`
- - Or `cd` into your existing `Lemp-PS-Commander` directory and run: `git pull && ./cp`
- - Pay attention, there may be some items that may need your input

## Usage:

### Creating a new site
- In Shell Run `new-site "ACCOUNTNAME" "ACCOUNTPASSWORD" "MAINDOMAIN" "REDIRECTTOWWW?" "ADDITIONALDOMAINS" "ADMINEMAIL" "DATABASENEEDED?" "DATABASEUSERNAME" "DATABASEUSERPASSWORD" "DATABASENAME"`
- Honestly...  just run `./cp`
- Profit

### Install a Wordpress Site
- In Shell Run `install-wordpress ACCOUNTNAME` and follow the prompts
- Profit

### Manually Update the server and All Wordpress Installs
- In shell run `updater`

### Terminate an Account
- In shell Run `terminate ACCOUNT`
- - It will backup the account and place the backup file in `/home` root

### Restore an Account
- In shell Run `restore-account ACCOUNTNAME YYYY-MM-DD`
- - Only run this for terminated accounts.
- - It will recreate the entire account

### Manually Scan the Server for malware or virii
- In Shell Run `scanner`

### Manually Run an Account Backup
- In Shell Run:
- - `account-backup` #will backup all sites and databases
- - `account-backup USER` #will backup just the users site and databases

### Manually generate and request SSL certificates
- Domain(s) to create certs for will need to be pointed to the server first
- In Shell Run: 
- - `gen-cert ACCOUNTNAME` #will generate and install a certificate for the main domain only
- - `gen-cert ACCOUNTNAME DOMAINS` #Pass a quoted comma-delimited list of DOMAINS, and will generate and install a cert for all applicable
- - - For Example: `gen-cert getyou "getyou.onl,www.getyou.onl,getme.onl"` #Will generate and install certs for the getyou account, with the listed domains

### Manually Optimize all sites images
- In Shell Run:
- - `optimages` #will optimize all images for all sites
- - `optimages ACCOUTNAME` #will optimize all images for the specified account

### Manually Clear Site Caches
- In Shell Run:
- - `clear-cache` #will clear server-side caches for all sites
- - `clear-cache ACCOUTNAME` #will clear server-side caches for the specified account

### Manually Prime Site Caches
- In Shell Run:
- - `site-primer` #will prime server-side caches for all sites
- - `site-primer ACCOUTNAME` #will prime server-side caches for the specified account

### WP-CLI
- In Shell Run:
- - `wp Command`
- - - See here for what you can do: http://wp-cli.org/

### Restart Services
- In Shell Run `restart-commander`
- - NOTE: If you are utilizing memcached, this will clear that out

### Clean Up Logs
- In Shell Run `clean-up`
- - - NOTE: only cleans up system logs

# IN THE WORKS - in order of importance
- Account Suspension
- Sub-Domain & Parked Domain Support

# Some Notes on Caching
- The script and the code contained here assumes that you know how to build and install a website on a server.  We make no assumptions that you know how to code, nor any assumptions that you know how to code properly.  We do however, make the assumption that these services will perform as well as the equipment it is installed on.
- With that out of the way, we have included a couple configurations for a couple Wordpress Caching Plugins, that we have found to be the best at what they do.  Caching
- WP Super Cache & W3 Total Cache.
- For WP Super Cache, please change the cache file location to `/media/cache/YOUR_SITES_ACCOUNT_NAME`
- For W3 Total Cache, add the following line to your wp-config.php file: `define('W3TC_CACHE_DIR', '/media/cache/YOUR_SITES_ACCOUNT_NAME/wtc_cache');`
- - This works best when you mount `/media/cache` as a Ram Disk
